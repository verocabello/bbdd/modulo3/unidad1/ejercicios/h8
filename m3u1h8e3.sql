﻿DROP DATABASE IF EXISTS m3u1h8e3;
CREATE DATABASE m3u1h8e3;
USE m3u1h8e3;

-- ESPECIE
CREATE OR REPLACE TABLE especie(
  nombre varchar(50),
  caracteristicas int,
  PRIMARY KEY(nombre)
);

-- MARIPOSA
CREATE OR REPLACE TABLE mariposa(
  nombrecientifico varchar(50),
  origen varchar(50),
  habitat varchar(50),
  esperanzavida int,
  nombre varchar(50),
  PRIMARY KEY(nombrecientifico)
);

-- COLORES
CREATE OR REPLACE TABLE colores(
  nombrecientifico varchar(50),
  colores int,
  PRIMARY KEY(nombrecientifico,colores)
);

-- EJEMPLAR
CREATE OR REPLACE TABLE ejemplar(
  nombrecientifico varchar(50),
  numero int,
  procedencia int,
  tamaño int,
  codigo int,
  PRIMARY KEY(nombrecientifico,numero)
);

-- COLECCION
CREATE OR REPLACE TABLE coleccion(
  codigo int,
  precioestimado int,
  ubicacion int,
  PRIMARY KEY(codigo)
);

-- PERSONA
CREATE OR REPLACE TABLE persona(
  dni char(9),
  nombre varchar(50),
  direccion int,
  telefono int,
  codigo int,
  esprincipal int,
  PRIMARY KEY(dni)
);

-- ESPECIECOLECCION
CREATE OR REPLACE TABLE especiecoleccion(
  nombre varchar(50),
  codigo int,
  mejorejemplar int,
  PRIMARY KEY(nombre,codigo)
);


/** creacion de las claves ajenas **/
  -- colores
  ALTER TABLE colores
    ADD CONSTRAINT fkcoloresmariposa
    FOREIGN KEY (nombrecientifico)
    REFERENCES mariposa(nombrecientifico);

  -- ejemplar
  ALTER TABLE ejemplar
    ADD CONSTRAINT fkejemplarmariposa
    FOREIGN KEY (nombrecientifico)
    REFERENCES mariposa(nombrecientifico),
    ADD CONSTRAINT fkejemplarcoleccion
    FOREIGN KEY (codigo)
    REFERENCES coleccion(codigo);
  
  -- persona
  ALTER TABLE persona
    ADD CONSTRAINT fkpersonacoleccion
    FOREIGN KEY (codigo)
    REFERENCES coleccion(codigo);

  -- especiecoleccion
  ALTER TABLE especiecoleccion
    ADD CONSTRAINT fkespeciecoleccionespecie
    FOREIGN KEY (nombre)
    REFERENCES especie(nombre),
    ADD CONSTRAINT fkespeciecoleccioncoleccion
    FOREIGN KEY (codigo)
    REFERENCES coleccion(codigo);
