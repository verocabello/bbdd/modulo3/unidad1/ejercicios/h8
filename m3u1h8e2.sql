﻿DROP DATABASE IF EXISTS m3u1h8e2;
CREATE DATABASE m3u1h8e2;
USE m3u1h8e2;

-- ZONA URBANA
CREATE OR REPLACE TABLE zonaurbana(
  nombrezona int AUTO_INCREMENT,
  categoria char(1),
  PRIMARY KEY(nombrezona)
 );

-- CASA PARTICULAR
CREATE OR REPLACE TABLE casaparticular(
  nombrezona int,
  numero int,
  m2 int,
  PRIMARY KEY(nombrezona,numero)
 );

-- PERSONA
CREATE OR REPLACE TABLE persona(
  dni char(9),
  nombre varchar(50),
  edad tinyint,
  nombrezonavivo int,
  numerovivo int,
  planta int,
  puerta char(1),
  calle varchar(50),
  numero int,
  PRIMARY KEY(dni)
 );

-- POSEEC
  CREATE TABLE poseec(
    dni char(9),
    nombrezona int,
    numero int,
    PRIMARY KEY(dni,nombrezona,numero)
  );

-- BLOQUE CASAS
  CREATE TABLE bloquecasas(
    calle varchar(50),
    numero int,
    npisos int,
    nombrezona int,
    PRIMARY KEY(calle,numero)
  );

-- PISO
  CREATE TABLE piso(
    planta int,
    puerta char(1),
    calle varchar(50),
    numero int,
    m2 int,
    PRIMARY KEY(planta,puerta,calle,numero)
  );

 -- POSEEP
  CREATE TABLE poseep(
    planta int,
    puerta char(1),
    calle varchar(50),
    numero int,
    dni char(9),
    PRIMARY KEY(planta,puerta,calle,numero,dni)
  );


/** creacion de las claves ajenas */
   -- casa particular
  ALTER TABLE casaparticular
    ADD CONSTRAINT fkcasaparticularzonaurbana
    FOREIGN KEY(nombrezona) 
    REFERENCES zonaurbana(nombrezona);

   -- persona
  ALTER TABLE persona
    ADD CONSTRAINT fkpersonapiso
    FOREIGN KEY (planta,puerta,calle,numero)
    REFERENCES piso(planta,puerta,calle,numero),
    ADD CONSTRAINT fkpersonacasaparticular
    FOREIGN KEY (nombrezonavivo,numerovivo)
    REFERENCES casaparticular(nombrezona,numero);

  -- poseec
  ALTER TABLE poseec
    ADD CONSTRAINT fkposeecpersona
    FOREIGN KEY (dni)
    REFERENCES persona(dni),
    ADD CONSTRAINT fkposeeccasaparticular
    FOREIGN KEY (nombrezona,numero)
    REFERENCES casaparticular(nombrezona,numero);
    
  -- bloque casas
  ALTER TABLE bloquecasas
    ADD CONSTRAINT fkboquecasaszonaurbana
    FOREIGN KEY (nombrezona)
    REFERENCES zonaurbana(nombrezona);

  -- piso
  ALTER TABLE piso
    ADD CONSTRAINT fkpisobloque
    FOREIGN KEY (calle,numero)
    REFERENCES bloquecasas(calle,numero);

  -- poseep
  ALTER TABLE poseep
    ADD CONSTRAINT fkposeeppiso
    FOREIGN KEY (planta,puerta,calle,numero)
    REFERENCES piso(planta,puerta,calle,numero),
    ADD CONSTRAINT fkposeeppersona
    FOREIGN KEY (dni)
    REFERENCES persona(dni);

